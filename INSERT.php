<?php
print_r(PDO::getAvailableDrivers());
$host = "localhost";
$dbname= "atomic_project_b37_sumona";
$user = "root";
$pass="";

try {

# MySQL with PDO_MYSQL
    $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
    $DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

}
catch(PDOException $e)
{
    echo "I'm sorry, Dave. I'm afraid I can't do that.";

    file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);

}

$data = array('Book_Title'=>'PHP BASIC LEARNING', 'Author_Name'=>'SUMONA');

$STH = $DBH->prepare("insert into BookTable(Book_Title,Author_Name) value (:Book_Title,:Author_Name)");
$STH->execute($data);

var_dump($STH);
